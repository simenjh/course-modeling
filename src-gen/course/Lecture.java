/**
 */
package course;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lecture</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.Lecture#getLectureHours <em>Lecture Hours</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getLecture()
 * @model
 * @generated
 */
public interface Lecture extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecture Hours</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hours</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hours</em>' attribute list.
	 * @see course.CoursePackage#getLecture_LectureHours()
	 * @model
	 * @generated
	 */
	EList<String> getLectureHours();

} // Lecture
