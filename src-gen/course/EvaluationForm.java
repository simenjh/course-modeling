/**
 */
package course;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.EvaluationForm#getConsistsOf <em>Consists Of</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getEvaluationForm()
 * @model
 * @generated
 */
public interface EvaluationForm extends EObject {
	/**
	 * Returns the value of the '<em><b>Consists Of</b></em>' reference list.
	 * The list contents are of type {@link course.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consists Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consists Of</em>' reference list.
	 * @see course.CoursePackage#getEvaluationForm_ConsistsOf()
	 * @model
	 * @generated
	 */
	EList<Activity> getConsistsOf();

} // EvaluationForm
