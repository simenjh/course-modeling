/**
 */
package course;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Table</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see course.CoursePackage#getTimeTable()
 * @model
 * @generated
 */
public interface TimeTable extends EObject {
} // TimeTable
