/**
 */
package course;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Employee</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.Employee#getEmployeeNr <em>Employee Nr</em>}</li>
 *   <li>{@link course.Employee#getLectures <em>Lectures</em>}</li>
 *   <li>{@link course.Employee#getCoordinates <em>Coordinates</em>}</li>
 *   <li>{@link course.Employee#getHasRole <em>Has Role</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getEmployee()
 * @model
 * @generated
 */
public interface Employee extends Person {
	/**
	 * Returns the value of the '<em><b>Employee Nr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Employee Nr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Employee Nr</em>' attribute.
	 * @see #setEmployeeNr(int)
	 * @see course.CoursePackage#getEmployee_EmployeeNr()
	 * @model
	 * @generated
	 */
	int getEmployeeNr();

	/**
	 * Sets the value of the '{@link course.Employee#getEmployeeNr <em>Employee Nr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Employee Nr</em>' attribute.
	 * @see #getEmployeeNr()
	 * @generated
	 */
	void setEmployeeNr(int value);

	/**
	 * Returns the value of the '<em><b>Lectures</b></em>' reference list.
	 * The list contents are of type {@link course.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link course.CourseInstance#getHasLecturers <em>Has Lecturers</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lectures</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lectures</em>' reference list.
	 * @see course.CoursePackage#getEmployee_Lectures()
	 * @see course.CourseInstance#getHasLecturers
	 * @model opposite="hasLecturers"
	 * @generated
	 */
	EList<CourseInstance> getLectures();

	/**
	 * Returns the value of the '<em><b>Coordinates</b></em>' reference list.
	 * The list contents are of type {@link course.CourseInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coordinates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coordinates</em>' reference list.
	 * @see course.CoursePackage#getEmployee_Coordinates()
	 * @model
	 * @generated
	 */
	EList<CourseInstance> getCoordinates();

	/**
	 * Returns the value of the '<em><b>Has Role</b></em>' reference list.
	 * The list contents are of type {@link course.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Role</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Role</em>' reference list.
	 * @see course.CoursePackage#getEmployee_HasRole()
	 * @model
	 * @generated
	 */
	EList<Role> getHasRole();

} // Employee
