/**
 */
package course.impl;

import course.CourseInstance;
import course.CoursePackage;
import course.Employee;
import course.Role;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Employee</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.EmployeeImpl#getEmployeeNr <em>Employee Nr</em>}</li>
 *   <li>{@link course.impl.EmployeeImpl#getLectures <em>Lectures</em>}</li>
 *   <li>{@link course.impl.EmployeeImpl#getCoordinates <em>Coordinates</em>}</li>
 *   <li>{@link course.impl.EmployeeImpl#getHasRole <em>Has Role</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EmployeeImpl extends PersonImpl implements Employee {
	/**
	 * The default value of the '{@link #getEmployeeNr() <em>Employee Nr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmployeeNr()
	 * @generated
	 * @ordered
	 */
	protected static final int EMPLOYEE_NR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEmployeeNr() <em>Employee Nr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmployeeNr()
	 * @generated
	 * @ordered
	 */
	protected int employeeNr = EMPLOYEE_NR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLectures() <em>Lectures</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectures()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> lectures;

	/**
	 * The cached value of the '{@link #getCoordinates() <em>Coordinates</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoordinates()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseInstance> coordinates;

	/**
	 * The cached value of the '{@link #getHasRole() <em>Has Role</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasRole()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> hasRole;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmployeeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.EMPLOYEE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEmployeeNr() {
		return employeeNr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmployeeNr(int newEmployeeNr) {
		int oldEmployeeNr = employeeNr;
		employeeNr = newEmployeeNr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.EMPLOYEE__EMPLOYEE_NR, oldEmployeeNr,
					employeeNr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getLectures() {
		if (lectures == null) {
			lectures = new EObjectWithInverseResolvingEList.ManyInverse<CourseInstance>(CourseInstance.class, this,
					CoursePackage.EMPLOYEE__LECTURES, CoursePackage.COURSE_INSTANCE__HAS_LECTURERS);
		}
		return lectures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CourseInstance> getCoordinates() {
		if (coordinates == null) {
			coordinates = new EObjectResolvingEList<CourseInstance>(CourseInstance.class, this,
					CoursePackage.EMPLOYEE__COORDINATES);
		}
		return coordinates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getHasRole() {
		if (hasRole == null) {
			hasRole = new EObjectResolvingEList<Role>(Role.class, this, CoursePackage.EMPLOYEE__HAS_ROLE);
		}
		return hasRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursePackage.EMPLOYEE__LECTURES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getLectures()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursePackage.EMPLOYEE__LECTURES:
			return ((InternalEList<?>) getLectures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursePackage.EMPLOYEE__EMPLOYEE_NR:
			return getEmployeeNr();
		case CoursePackage.EMPLOYEE__LECTURES:
			return getLectures();
		case CoursePackage.EMPLOYEE__COORDINATES:
			return getCoordinates();
		case CoursePackage.EMPLOYEE__HAS_ROLE:
			return getHasRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursePackage.EMPLOYEE__EMPLOYEE_NR:
			setEmployeeNr((Integer) newValue);
			return;
		case CoursePackage.EMPLOYEE__LECTURES:
			getLectures().clear();
			getLectures().addAll((Collection<? extends CourseInstance>) newValue);
			return;
		case CoursePackage.EMPLOYEE__COORDINATES:
			getCoordinates().clear();
			getCoordinates().addAll((Collection<? extends CourseInstance>) newValue);
			return;
		case CoursePackage.EMPLOYEE__HAS_ROLE:
			getHasRole().clear();
			getHasRole().addAll((Collection<? extends Role>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursePackage.EMPLOYEE__EMPLOYEE_NR:
			setEmployeeNr(EMPLOYEE_NR_EDEFAULT);
			return;
		case CoursePackage.EMPLOYEE__LECTURES:
			getLectures().clear();
			return;
		case CoursePackage.EMPLOYEE__COORDINATES:
			getCoordinates().clear();
			return;
		case CoursePackage.EMPLOYEE__HAS_ROLE:
			getHasRole().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursePackage.EMPLOYEE__EMPLOYEE_NR:
			return employeeNr != EMPLOYEE_NR_EDEFAULT;
		case CoursePackage.EMPLOYEE__LECTURES:
			return lectures != null && !lectures.isEmpty();
		case CoursePackage.EMPLOYEE__COORDINATES:
			return coordinates != null && !coordinates.isEmpty();
		case CoursePackage.EMPLOYEE__HAS_ROLE:
			return hasRole != null && !hasRole.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (employeeNr: ");
		result.append(employeeNr);
		result.append(')');
		return result.toString();
	}

} //EmployeeImpl
