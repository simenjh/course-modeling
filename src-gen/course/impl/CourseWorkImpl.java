/**
 */
package course.impl;

import course.CoursePackage;
import course.CourseWork;
import course.Lab;
import course.Lecture;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Work</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.CourseWorkImpl#getHasLab <em>Has Lab</em>}</li>
 *   <li>{@link course.impl.CourseWorkImpl#getHasLecture <em>Has Lecture</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseWorkImpl extends MinimalEObjectImpl.Container implements CourseWork {
	/**
	 * The cached value of the '{@link #getHasLab() <em>Has Lab</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLab()
	 * @generated
	 * @ordered
	 */
	protected EList<Lab> hasLab;

	/**
	 * The cached value of the '{@link #getHasLecture() <em>Has Lecture</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLecture()
	 * @generated
	 * @ordered
	 */
	protected EList<Lecture> hasLecture;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseWorkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.COURSE_WORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Lab> getHasLab() {
		if (hasLab == null) {
			hasLab = new EObjectResolvingEList<Lab>(Lab.class, this, CoursePackage.COURSE_WORK__HAS_LAB);
		}
		return hasLab;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Lecture> getHasLecture() {
		if (hasLecture == null) {
			hasLecture = new EObjectResolvingEList<Lecture>(Lecture.class, this,
					CoursePackage.COURSE_WORK__HAS_LECTURE);
		}
		return hasLecture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursePackage.COURSE_WORK__HAS_LAB:
			return getHasLab();
		case CoursePackage.COURSE_WORK__HAS_LECTURE:
			return getHasLecture();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursePackage.COURSE_WORK__HAS_LAB:
			getHasLab().clear();
			getHasLab().addAll((Collection<? extends Lab>) newValue);
			return;
		case CoursePackage.COURSE_WORK__HAS_LECTURE:
			getHasLecture().clear();
			getHasLecture().addAll((Collection<? extends Lecture>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursePackage.COURSE_WORK__HAS_LAB:
			getHasLab().clear();
			return;
		case CoursePackage.COURSE_WORK__HAS_LECTURE:
			getHasLecture().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursePackage.COURSE_WORK__HAS_LAB:
			return hasLab != null && !hasLab.isEmpty();
		case CoursePackage.COURSE_WORK__HAS_LECTURE:
			return hasLecture != null && !hasLecture.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CourseWorkImpl
