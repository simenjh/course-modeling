/**
 */
package course.impl;

import course.CoursePackage;
import course.TimeTable;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimeTableImpl extends MinimalEObjectImpl.Container implements TimeTable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.TIME_TABLE;
	}

} //TimeTableImpl
