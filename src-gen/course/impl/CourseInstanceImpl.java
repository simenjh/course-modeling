/**
 */
package course.impl;

import course.CourseInstance;
import course.CoursePackage;
import course.CourseWork;
import course.Employee;
import course.EvaluationForm;
import course.TimeTable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.CourseInstanceImpl#getHasWork <em>Has Work</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getHasLecturers <em>Has Lecturers</em>}</li>
 *   <li>{@link course.impl.CourseInstanceImpl#getSemester <em>Semester</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends CourseImpl implements CourseInstance {
	/**
	 * The cached value of the '{@link #getHasWork() <em>Has Work</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasWork()
	 * @generated
	 * @ordered
	 */
	protected CourseWork hasWork;

	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected TimeTable timetable;

	/**
	 * The cached value of the '{@link #getEvaluationform() <em>Evaluationform</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationform()
	 * @generated
	 * @ordered
	 */
	protected EvaluationForm evaluationform;

	/**
	 * The cached value of the '{@link #getHasLecturers() <em>Has Lecturers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLecturers()
	 * @generated
	 * @ordered
	 */
	protected EList<Employee> hasLecturers;

	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMESTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected String semester = SEMESTER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork getHasWork() {
		if (hasWork != null && hasWork.eIsProxy()) {
			InternalEObject oldHasWork = (InternalEObject) hasWork;
			hasWork = (CourseWork) eResolveProxy(oldHasWork);
			if (hasWork != oldHasWork) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE_INSTANCE__HAS_WORK,
							oldHasWork, hasWork));
			}
		}
		return hasWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork basicGetHasWork() {
		return hasWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasWork(CourseWork newHasWork) {
		CourseWork oldHasWork = hasWork;
		hasWork = newHasWork;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__HAS_WORK, oldHasWork,
					hasWork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeTable getTimetable() {
		if (timetable != null && timetable.eIsProxy()) {
			InternalEObject oldTimetable = (InternalEObject) timetable;
			timetable = (TimeTable) eResolveProxy(oldTimetable);
			if (timetable != oldTimetable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE_INSTANCE__TIMETABLE,
							oldTimetable, timetable));
			}
		}
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeTable basicGetTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(TimeTable newTimetable) {
		TimeTable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__TIMETABLE,
					oldTimetable, timetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationForm getEvaluationform() {
		if (evaluationform != null && evaluationform.eIsProxy()) {
			InternalEObject oldEvaluationform = (InternalEObject) evaluationform;
			evaluationform = (EvaluationForm) eResolveProxy(oldEvaluationform);
			if (evaluationform != oldEvaluationform) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							CoursePackage.COURSE_INSTANCE__EVALUATIONFORM, oldEvaluationform, evaluationform));
			}
		}
		return evaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationForm basicGetEvaluationform() {
		return evaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluationform(EvaluationForm newEvaluationform) {
		EvaluationForm oldEvaluationform = evaluationform;
		evaluationform = newEvaluationform;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__EVALUATIONFORM,
					oldEvaluationform, evaluationform));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Employee> getHasLecturers() {
		if (hasLecturers == null) {
			hasLecturers = new EObjectWithInverseResolvingEList.ManyInverse<Employee>(Employee.class, this,
					CoursePackage.COURSE_INSTANCE__HAS_LECTURERS, CoursePackage.EMPLOYEE__LECTURES);
		}
		return hasLecturers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(String newSemester) {
		String oldSemester = semester;
		semester = newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE_INSTANCE__SEMESTER, oldSemester,
					semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursePackage.COURSE_INSTANCE__HAS_LECTURERS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getHasLecturers()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursePackage.COURSE_INSTANCE__HAS_LECTURERS:
			return ((InternalEList<?>) getHasLecturers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursePackage.COURSE_INSTANCE__HAS_WORK:
			if (resolve)
				return getHasWork();
			return basicGetHasWork();
		case CoursePackage.COURSE_INSTANCE__TIMETABLE:
			if (resolve)
				return getTimetable();
			return basicGetTimetable();
		case CoursePackage.COURSE_INSTANCE__EVALUATIONFORM:
			if (resolve)
				return getEvaluationform();
			return basicGetEvaluationform();
		case CoursePackage.COURSE_INSTANCE__HAS_LECTURERS:
			return getHasLecturers();
		case CoursePackage.COURSE_INSTANCE__SEMESTER:
			return getSemester();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursePackage.COURSE_INSTANCE__HAS_WORK:
			setHasWork((CourseWork) newValue);
			return;
		case CoursePackage.COURSE_INSTANCE__TIMETABLE:
			setTimetable((TimeTable) newValue);
			return;
		case CoursePackage.COURSE_INSTANCE__EVALUATIONFORM:
			setEvaluationform((EvaluationForm) newValue);
			return;
		case CoursePackage.COURSE_INSTANCE__HAS_LECTURERS:
			getHasLecturers().clear();
			getHasLecturers().addAll((Collection<? extends Employee>) newValue);
			return;
		case CoursePackage.COURSE_INSTANCE__SEMESTER:
			setSemester((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursePackage.COURSE_INSTANCE__HAS_WORK:
			setHasWork((CourseWork) null);
			return;
		case CoursePackage.COURSE_INSTANCE__TIMETABLE:
			setTimetable((TimeTable) null);
			return;
		case CoursePackage.COURSE_INSTANCE__EVALUATIONFORM:
			setEvaluationform((EvaluationForm) null);
			return;
		case CoursePackage.COURSE_INSTANCE__HAS_LECTURERS:
			getHasLecturers().clear();
			return;
		case CoursePackage.COURSE_INSTANCE__SEMESTER:
			setSemester(SEMESTER_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursePackage.COURSE_INSTANCE__HAS_WORK:
			return hasWork != null;
		case CoursePackage.COURSE_INSTANCE__TIMETABLE:
			return timetable != null;
		case CoursePackage.COURSE_INSTANCE__EVALUATIONFORM:
			return evaluationform != null;
		case CoursePackage.COURSE_INSTANCE__HAS_LECTURERS:
			return hasLecturers != null && !hasLecturers.isEmpty();
		case CoursePackage.COURSE_INSTANCE__SEMESTER:
			return SEMESTER_EDEFAULT == null ? semester != null : !SEMESTER_EDEFAULT.equals(semester);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl
