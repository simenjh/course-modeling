/**
 */
package course.impl;

import course.Course;
import course.CoursePackage;
import course.Department;
import course.StudyProgram;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link course.impl.CourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getBelongsTo <em>Belongs To</em>}</li>
 *   <li>{@link course.impl.CourseImpl#getCourseBelongsTo <em>Course Belongs To</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBelongsTo() <em>Belongs To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBelongsTo()
	 * @generated
	 * @ordered
	 */
	protected Department belongsTo;

	/**
	 * The cached value of the '{@link #getCourseBelongsTo() <em>Course Belongs To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseBelongsTo()
	 * @generated
	 * @ordered
	 */
	protected StudyProgram courseBelongsTo;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursePackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getBelongsTo() {
		if (belongsTo != null && belongsTo.eIsProxy()) {
			InternalEObject oldBelongsTo = (InternalEObject) belongsTo;
			belongsTo = (Department) eResolveProxy(oldBelongsTo);
			if (belongsTo != oldBelongsTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE__BELONGS_TO,
							oldBelongsTo, belongsTo));
			}
		}
		return belongsTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department basicGetBelongsTo() {
		return belongsTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBelongsTo(Department newBelongsTo, NotificationChain msgs) {
		Department oldBelongsTo = belongsTo;
		belongsTo = newBelongsTo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursePackage.COURSE__BELONGS_TO, oldBelongsTo, newBelongsTo);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBelongsTo(Department newBelongsTo) {
		if (newBelongsTo != belongsTo) {
			NotificationChain msgs = null;
			if (belongsTo != null)
				msgs = ((InternalEObject) belongsTo).eInverseRemove(this, CoursePackage.DEPARTMENT__HAS_COURSE,
						Department.class, msgs);
			if (newBelongsTo != null)
				msgs = ((InternalEObject) newBelongsTo).eInverseAdd(this, CoursePackage.DEPARTMENT__HAS_COURSE,
						Department.class, msgs);
			msgs = basicSetBelongsTo(newBelongsTo, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__BELONGS_TO, newBelongsTo,
					newBelongsTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgram getCourseBelongsTo() {
		if (courseBelongsTo != null && courseBelongsTo.eIsProxy()) {
			InternalEObject oldCourseBelongsTo = (InternalEObject) courseBelongsTo;
			courseBelongsTo = (StudyProgram) eResolveProxy(oldCourseBelongsTo);
			if (courseBelongsTo != oldCourseBelongsTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoursePackage.COURSE__COURSE_BELONGS_TO,
							oldCourseBelongsTo, courseBelongsTo));
			}
		}
		return courseBelongsTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyProgram basicGetCourseBelongsTo() {
		return courseBelongsTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseBelongsTo(StudyProgram newCourseBelongsTo, NotificationChain msgs) {
		StudyProgram oldCourseBelongsTo = courseBelongsTo;
		courseBelongsTo = newCourseBelongsTo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursePackage.COURSE__COURSE_BELONGS_TO, oldCourseBelongsTo, newCourseBelongsTo);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseBelongsTo(StudyProgram newCourseBelongsTo) {
		if (newCourseBelongsTo != courseBelongsTo) {
			NotificationChain msgs = null;
			if (courseBelongsTo != null)
				msgs = ((InternalEObject) courseBelongsTo).eInverseRemove(this, CoursePackage.STUDY_PROGRAM__HAS_COURSE,
						StudyProgram.class, msgs);
			if (newCourseBelongsTo != null)
				msgs = ((InternalEObject) newCourseBelongsTo).eInverseAdd(this, CoursePackage.STUDY_PROGRAM__HAS_COURSE,
						StudyProgram.class, msgs);
			msgs = basicSetCourseBelongsTo(newCourseBelongsTo, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursePackage.COURSE__COURSE_BELONGS_TO,
					newCourseBelongsTo, newCourseBelongsTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursePackage.COURSE__BELONGS_TO:
			if (belongsTo != null)
				msgs = ((InternalEObject) belongsTo).eInverseRemove(this, CoursePackage.DEPARTMENT__HAS_COURSE,
						Department.class, msgs);
			return basicSetBelongsTo((Department) otherEnd, msgs);
		case CoursePackage.COURSE__COURSE_BELONGS_TO:
			if (courseBelongsTo != null)
				msgs = ((InternalEObject) courseBelongsTo).eInverseRemove(this, CoursePackage.STUDY_PROGRAM__HAS_COURSE,
						StudyProgram.class, msgs);
			return basicSetCourseBelongsTo((StudyProgram) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursePackage.COURSE__BELONGS_TO:
			return basicSetBelongsTo(null, msgs);
		case CoursePackage.COURSE__COURSE_BELONGS_TO:
			return basicSetCourseBelongsTo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursePackage.COURSE__CODE:
			return getCode();
		case CoursePackage.COURSE__NAME:
			return getName();
		case CoursePackage.COURSE__CONTENT:
			return getContent();
		case CoursePackage.COURSE__BELONGS_TO:
			if (resolve)
				return getBelongsTo();
			return basicGetBelongsTo();
		case CoursePackage.COURSE__COURSE_BELONGS_TO:
			if (resolve)
				return getCourseBelongsTo();
			return basicGetCourseBelongsTo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursePackage.COURSE__CODE:
			setCode((String) newValue);
			return;
		case CoursePackage.COURSE__NAME:
			setName((String) newValue);
			return;
		case CoursePackage.COURSE__CONTENT:
			setContent((String) newValue);
			return;
		case CoursePackage.COURSE__BELONGS_TO:
			setBelongsTo((Department) newValue);
			return;
		case CoursePackage.COURSE__COURSE_BELONGS_TO:
			setCourseBelongsTo((StudyProgram) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursePackage.COURSE__CODE:
			setCode(CODE_EDEFAULT);
			return;
		case CoursePackage.COURSE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case CoursePackage.COURSE__CONTENT:
			setContent(CONTENT_EDEFAULT);
			return;
		case CoursePackage.COURSE__BELONGS_TO:
			setBelongsTo((Department) null);
			return;
		case CoursePackage.COURSE__COURSE_BELONGS_TO:
			setCourseBelongsTo((StudyProgram) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursePackage.COURSE__CODE:
			return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
		case CoursePackage.COURSE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case CoursePackage.COURSE__CONTENT:
			return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
		case CoursePackage.COURSE__BELONGS_TO:
			return belongsTo != null;
		case CoursePackage.COURSE__COURSE_BELONGS_TO:
			return courseBelongsTo != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", name: ");
		result.append(name);
		result.append(", content: ");
		result.append(content);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
