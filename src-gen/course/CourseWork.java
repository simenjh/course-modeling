/**
 */
package course;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Work</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.CourseWork#getHasLab <em>Has Lab</em>}</li>
 *   <li>{@link course.CourseWork#getHasLecture <em>Has Lecture</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getCourseWork()
 * @model
 * @generated
 */
public interface CourseWork extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Lab</b></em>' reference list.
	 * The list contents are of type {@link course.Lab}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Lab</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Lab</em>' reference list.
	 * @see course.CoursePackage#getCourseWork_HasLab()
	 * @model
	 * @generated
	 */
	EList<Lab> getHasLab();

	/**
	 * Returns the value of the '<em><b>Has Lecture</b></em>' reference list.
	 * The list contents are of type {@link course.Lecture}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Lecture</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Lecture</em>' reference list.
	 * @see course.CoursePackage#getCourseWork_HasLecture()
	 * @model
	 * @generated
	 */
	EList<Lecture> getHasLecture();

} // CourseWork
