/**
 */
package course;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link course.CourseInstance#getHasWork <em>Has Work</em>}</li>
 *   <li>{@link course.CourseInstance#getTimetable <em>Timetable</em>}</li>
 *   <li>{@link course.CourseInstance#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link course.CourseInstance#getHasLecturers <em>Has Lecturers</em>}</li>
 *   <li>{@link course.CourseInstance#getSemester <em>Semester</em>}</li>
 * </ul>
 *
 * @see course.CoursePackage#getCourseInstance()
 * @model
 * @generated
 */
public interface CourseInstance extends Course {
	/**
	 * Returns the value of the '<em><b>Has Work</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Work</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Work</em>' reference.
	 * @see #setHasWork(CourseWork)
	 * @see course.CoursePackage#getCourseInstance_HasWork()
	 * @model
	 * @generated
	 */
	CourseWork getHasWork();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getHasWork <em>Has Work</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Work</em>' reference.
	 * @see #getHasWork()
	 * @generated
	 */
	void setHasWork(CourseWork value);

	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' reference.
	 * @see #setTimetable(TimeTable)
	 * @see course.CoursePackage#getCourseInstance_Timetable()
	 * @model
	 * @generated
	 */
	TimeTable getTimetable();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getTimetable <em>Timetable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetable</em>' reference.
	 * @see #getTimetable()
	 * @generated
	 */
	void setTimetable(TimeTable value);

	/**
	 * Returns the value of the '<em><b>Evaluationform</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluationform</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluationform</em>' reference.
	 * @see #setEvaluationform(EvaluationForm)
	 * @see course.CoursePackage#getCourseInstance_Evaluationform()
	 * @model
	 * @generated
	 */
	EvaluationForm getEvaluationform();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getEvaluationform <em>Evaluationform</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluationform</em>' reference.
	 * @see #getEvaluationform()
	 * @generated
	 */
	void setEvaluationform(EvaluationForm value);

	/**
	 * Returns the value of the '<em><b>Has Lecturers</b></em>' reference list.
	 * The list contents are of type {@link course.Employee}.
	 * It is bidirectional and its opposite is '{@link course.Employee#getLectures <em>Lectures</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Lecturers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Lecturers</em>' reference list.
	 * @see course.CoursePackage#getCourseInstance_HasLecturers()
	 * @see course.Employee#getLectures
	 * @model opposite="lectures"
	 * @generated
	 */
	EList<Employee> getHasLecturers();

	/**
	 * Returns the value of the '<em><b>Semester</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semester</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semester</em>' attribute.
	 * @see #setSemester(String)
	 * @see course.CoursePackage#getCourseInstance_Semester()
	 * @model
	 * @generated
	 */
	String getSemester();

	/**
	 * Sets the value of the '{@link course.CourseInstance#getSemester <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semester</em>' attribute.
	 * @see #getSemester()
	 * @generated
	 */
	void setSemester(String value);

} // CourseInstance
